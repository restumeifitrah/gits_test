package com.example.dell.newsapps;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Explode;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class Detail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

        // set an exit transition
            getWindow().setExitTransition(new Explode());
            // Apply activity transition
        } else {
            // Swap without transition
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        TextView title = (TextView) findViewById(R.id.textView2);
        TextView deskripsi = (TextView) findViewById(R.id.textView3);
        ImageView image = (ImageView) findViewById(R.id.imageView2);

        Intent intent = getIntent();

        title.setText(intent.getStringExtra("Nama"));
        deskripsi.setText(intent.getStringExtra("deskripsi"));
        Picasso.get().load(intent.getStringExtra("gambar")).into(image);

    }
}
