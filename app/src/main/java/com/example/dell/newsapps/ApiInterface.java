package com.example.dell.newsapps;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by DELL on 15/04/2019.
 */

public interface ApiInterface {


    @GET("top-headlines")
    Call<Model> getModel(@Query("country") String country,
                         @Query("category") String category,
                         @Query("apiKey") String apiKey);



}
