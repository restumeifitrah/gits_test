package com.example.dell.newsapps;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by DELL on 15/04/2019.
 */

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.MyViewHolder> {

    ArrayList<Model.Articles> mKontakList;

    public RecycleViewAdapter(ArrayList<Model.Articles> KontakList) {
        mKontakList = KontakList;
    }

    @Override
    public MyViewHolder onCreateViewHolder (ViewGroup parent, int viewType){
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        MyViewHolder mViewHolder = new MyViewHolder(mView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder (final MyViewHolder holder, final int position){



        holder.mTextViewNama.setText(mKontakList.get(position).getTitle() );



        Picasso.get().load(mKontakList.get(position).getUrlToImage()).into(holder.image);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(view.getContext(), Detail.class);

                mIntent.putExtra("Nama", mKontakList.get(position).getTitle());
                mIntent.putExtra("deskripsi", mKontakList.get(position).getDescription());
                mIntent.putExtra("gambar", mKontakList.get(position).getUrlToImage());


                view.getContext().startActivity(mIntent, ActivityOptions.makeSceneTransitionAnimation((Activity) view.getContext()).toBundle());
            }
        });
    }

    @Override
    public int getItemCount () {
        return mKontakList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextViewId, mTextViewNama;
        public ImageView image ;

        public MyViewHolder(View itemView) {
            super(itemView);
            image  = (ImageView)  itemView.findViewById(R.id.imageView);
            mTextViewNama = (TextView) itemView.findViewById(R.id.textView);

        }
}}
